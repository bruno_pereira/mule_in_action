package br.com.rivendel.mule;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

@Path("hellorest")
public class HelloWorldResource {

	@GET
	public String getWelcomeMsg() throws Exception {
		return "Hi MuleCookBook";
	}

}
