package br.com.rivendel.mule;

import javax.jws.WebService;

@WebService(endpointInterface = "br.com.rivendel.mule.HelloService", serviceName = "HelloService")
public class HelloServiceImpl implements HelloService {

	@Override
	public String hiMule(String str) throws Exception {
		return "Hello " + str;
	}

}
