package br.com.rivendel.mule;

import javax.jws.WebService;

@WebService
public interface HelloService {
	public String hiMule(String str) throws Exception;
}
